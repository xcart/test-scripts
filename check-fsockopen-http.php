<?php

$fp = fsockopen("example.com", 80, $errno, $errstr, 30);
if (!$fp) {
    echo "$errstr ($errno)<br />\n";
} else {
    $out = "GET /index.html HTTP/1.0\r\n";
    $out .= "Host: example.com\r\n";
    $out .="User-Agent: Mozilla/4.5 [en]\r\n";
//    $out .= "Cookie: is_robot=1\r\n";
    $out .= "\r\n";

    fwrite($fp, $out);
    while (!feof($fp)) {
        echo fgets($fp, 128);
    }
    fclose($fp);
}
