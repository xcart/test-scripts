<?php

#
# Function to get backtrace for debugging
#
function func_get_backtrace1($skip=0) {
    $result = array();
    if (!function_exists('debug_backtrace')) {
        $result[] = '[func_get_backtrace() is supported only for PHP version 4.3.0 or better]';
        return $result;
    }
    $trace = debug_backtrace();

    if (is_array($trace) && !empty($trace)) {
        if ($skip>0) {
            if ($skip < count($trace))
                $trace = array_splice($trace, $skip);
            else
                $trace = array();
        }

        foreach ($trace as $item) {
            if (!empty($item['file']))
                $result[] = $item['file'].':'.$item['line'];
        }
    }

    if (empty($result)) {
        $result[] = '[empty backtrace]';
    }

    return $result;
}

function strip_slashes_recursive1($variable)
{
    if (is_string($variable))
        return stripslashes($variable);
    if (is_array($variable))
        foreach($variable as $i => $value)
            $variable[ $i ] = strip_slashes_recursive1($value) ;
    
    return $variable ; 
}

#
# Write log entry to log/xcart.log file
#
function x_log1($logfile, $name, $str) {
    if ($fp = fopen($logfile, "a")) {

        $stack = func_get_backtrace1(1);

		$str = strip_slashes_recursive1($str);

        if (flock($fp, LOCK_EX)) {
            fwrite($fp, "\n\n <?php die(); ?>\n".date("r")."\n".$name.": ".var_export($str, true)."\n\n");
            fwrite($fp, "Backtrace:\n".implode("\n", $stack)."\n");
            flock($fp, LOCK_UN);
        }
        fclose($fp);
    }
}
?>
