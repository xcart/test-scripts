<?php

$fp = fsockopen("localhost", 25, $errno, $errstr, 30);
if (!$fp) {
    echo "$errstr ($errno)<br />\n";
} else {
    $out = "HELO localhost\r\n";
    $out .= "mail from: bit-bucket@x-cart.com\r\n";
    $out .= "rcpt to: supporters@x-cart.com\r\n";
    $out .= "data\r\n";
    $out .= "test\r\n.\r\n";
    $out .= "QUIT\r\n\r\n";

    fwrite($fp, $out);
    while (!feof($fp)) {
        echo fgets($fp, 128);
    }
    fclose($fp);
}
