<?php
$begin = getmicrotime();

error_reporting(E_ALL);

$mem = 256;

ini_set("memory_limit", $mem . "M");
ini_set("max_execution_time", "60");

echo "<html><body>";
echo "Memory limit: " . ini_get("memory_limit") . "<br>";
echo "Max execution time: " . ini_get("max_execution_time") . "<br>";
echo "Building very long string ... <br>";
flush();

$array = array();
for ($i = 1; $i < $mem; $i++) {
    echo "Adding 1M ... ";
    add($array);
    echo "[OK] Total: $i Mb, usage: " . memory_get_usage() . "<br>";
}

$array = array();
$end = getmicrotime();
echo "Execution time: "; echo  $end - $begin;
echo "</body></html>";

function getmicrotime() {

    list($usec, $sec) = explode(" ",microtime());
    return ((float)$usec + (float)$sec);
}


function add(&$array) {

    $array[] = str_repeat("*", 1024*1024);
}

