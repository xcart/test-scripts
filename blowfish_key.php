<?php

// Needed to not mess PHP and HTML
function func_table_header($key, $fields) {
$header = <<<EOF
<!DOCTYPE html>
<html>
<head>
<style>
td.field {
max-width: 800px;
overflow: hidden;
}
</style>
</head>
<body>
EOF;

    echo $header;
    echo "<table border='1'><tr><th>$key</th>";

    foreach ($fields as $field) {
        echo "<th>$field</th>";
        echo "<th>Cur key $field</th>";
        echo "<th>Old key $field</th>";
    }

    echo "</tr>";
}

function func_table_fields($fields) {
    $return = '';
    foreach ($fields as $field) {
        $return .= "<td class='field'>$field</td>";
    }

    return $return;
}

function func_table_footer() {
    echo "</table>";
}

require_once("./auth.php");
require_once("./include/blowfish.php");
x_load("crypt");

$table = 'order_extras';
$rewrite_fields = true;
$limit = '25000';
$display_issues_only = true;
$blowfish_key_old = 'N6v8SkGEaJy0XLW7y0lYa5qCfDfX7JCW';
$start = 256783;
$additinal_keys = array(
    'khash' => array('ccdata', 'advinfo')
);

$blowfish_key_cur = $blowfish_key;
$table_name = $xcart_tbl_prefix . $table;
$key = $bf_crypted_tables[$table]['key'];
$fields = $bf_crypted_tables[$table]['fields'];
$where = $key . ' > ' . $start;

if(isset($bf_crypted_tables[$table]['where'])) {
    $where .= $bf_crypted_tables[$table]['where'];
}

$where .= ' AND  ( 0 ';
foreach ($fields as $field) {
    $where .= " || " . $field . " != '' ";
}
$where .= ' )';

$data = func_query($test = "SELECT $key,".implode(array_keys($additinal_keys))."," . implode(",", $fields) . " FROM $table_name WHERE $where ORDER BY $key LIMIT $limit");
#echo $test;
if(!$rewrite_fields)
func_table_header($key, $fields);

foreach ($data as $value) {
    $is_issue = false;

    $line = "<tr>";
    $line .="<td class='$class'>".$value[$key]."</td>";
    
    foreach ($fields as $field) {
        if(in_array($field, array_keys($additinal_keys)))
            continue;

        $table_fields = array();
        $table_fields[0] = $value[$field];
        $table_fields[1] = text_decrypt($value[$field]);

        $table_fields[2] = '';
        if($table_fields[1] === false || is_null($table_fields[1])) {
            $table_fields[2] = text_decrypt($value[$field], $blowfish_key_old);

            if($table_fields[2] !== false && !is_null($table_fields[2])) {
                $is_issue = true;
            }
        }

        if($rewrite_fields && $is_issue) {
            $query = "UPDATE $table_name SET ".$field." = '".text_crypt($table_fields[2])."' WHERE $key = '".$value[$key]."'";

            foreach ($additinal_keys as $add_key => $add_val) {
                if(isset($value[$add_key]) && in_array($value[$add_key], $add_val)) {
                    $query .= " AND $add_key = '".$value[$add_key]."'";
                }
            }

            echo $query . "<br />";
            db_query($query);
            continue;
        }

        $line .= func_table_fields($table_fields);
    }
    
    $line .= "</tr>";

    if(!$rewrite_fields && (!$display_issues_only || $is_issue)) {
        echo $line;
    }
}

if(!$rewrite_fields)
func_table_footer();

echo "Done";
