<?php

ini_set("max_execution_time","3600");

function __curl_headers()
{
    static $headers = '';

    $args = func_get_args();
    if (count($args) == 1) {
        $return = '';
        if ($args[0] == true) $return = $headers;
        $headers = '';
        return $return;
    }

    if (trim($args[1]) != '') $headers .= $args[1];
    return strlen($args[1]);
}

$host = "https://bustedoffroad.x-checkout.com/api.php";

$headers = array();
$headers[] = "Content-Type: application/x-www-form-urlencoded";

$join = "&";
 
$sh = curl_init();

curl_setopt($sh, CURLOPT_URL, $host);

curl_setopt ($sh, CURLOPT_HEADER, 0);
curl_setopt ($sh, CURLOPT_HTTPHEADER, $headers);

curl_setopt ($sh, CURLOPT_TIMEOUT, 120);

curl_setopt ($sh, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt ($sh, CURLOPT_SSL_VERIFYHOST, 1);

curl_setopt ($sh, CURLOPT_POST, 1);

curl_setopt ($sh, CURLOPT_RETURNTRANSFER, 1);
curl_setopt ($sh, CURLOPT_HEADERFUNCTION, '__curl_headers');


#$start_time = time();
#echo "Start connection time: ".$start_time."<br />";

$body = curl_exec( $sh );

#$diff = time() - $start_time;
#echo "Connection time: ".$diff." seconds <br /><br /><br />";

echo "Trying to connect to the host: ". $host ."<br /><br />";

$error = curl_error( $sh );
if( !empty( $error )) {

	print("FAIL: $error\n");
	exit(1);

} 
else {

	print("Server reply: " . trim($body) . "\n");
	curl_close($sh);

#	echo "<br /><br /> Server Addr: ".$_SERVER['SERVER_ADDR'];

	exit(0);
}
?>
