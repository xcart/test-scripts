<?php

#echo "It works";

$send_to = $_GET['send_to'];
$mode = $_GET['mode'];

$start_time = time();

if ($send_to == "") exit;

if ($mode == ""){
	if (mail($send_to, "Standard mail test", "Test message body",
    	"From: bitbucket@x-cart.com\r\n"
   		."Reply-To: bitbucket@x-cart.com\r\n"
   		."X-Mailer: PHP/" . phpversion()))
		echo "Mail sent";
	else 
		echo "Mail not sent";
}
elseif ($mode == "fkey"){
	if (mail($send_to, "Test -f parameter", "Test mail with -f sendmail option", "From: webmaster@".$SERVER_NAME, "-fwebmaster@".$SERVER_NAME))
		echo "Mail with -f sendmail parameter sent";
	else 
		echo "Mail with -f sendmail parameter not sent";
}
elseif ($mode == "simple"){
	if (mail($send_to, "Test simple mail", "Simple mail body"))
		echo "Simple mail sent";
	else 
		echo "Simple mail not sent";

$working_time = time() - $start_time;

echo "<br /><br /> $working_time seconds";

}

?>
