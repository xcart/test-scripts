<?php
$begin = microtime(true);

$ch = curl_init("https://www.ups.com:443/ups.app/xml/Rate");

curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
// curl_setopt($ch, CURLOPT_SSLVERSION, 3);

// force 1.2
//     curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
//     curl_setopt($ch, CURLOPT_SSLVERSION, 6);

curl_exec($ch);

$end = microtime(true);

echo "Execution time: "; echo  $end - $begin; echo " seconds<br>\n";
print_r(curl_error($ch));
curl_close ($ch);
