<?php
require_once("./auth.php");
x_load("mail");
require_once $xcart_dir."/include/xcartmailer.php";

require_once $xcart_dir."/include/class.pop3.php";

$pop = new POP3();
$pop->Authorise($config['Email']['smtp_server'], 110, 30, $config['Email']['smtp_user'], $config['Email']['smtp_password'], 1);

$smtp = new XcartMailer();
$port = intval($config['Email']['smtp_port']);

	# Initiate connection.
	if (!$smtp->Connect($config['Email']['smtp_server'], $port)) {
		$result[] = func_get_smtp_server_error($smtp, 'Failed to connect to SMTP server', 'errno', 'errstr');
		$smtp->Close();
	}

	# Send greeting.
	if (!$smtp->Hello()) {
		$result[] = func_get_smtp_server_error($smtp, 'Failed to complete SMTP handshake');
		$smtp->Close();
	}

	# Perform SMTP authentication.
	if (!empty($config['Email']['smtp_user'])) {
		$smtp->setAuthMethod($config['Email']['smtp_auth_method']);
		if (!$smtp->Authenticate($config['Email']['smtp_user'], $config['Email']['smtp_password'])) {
			$result = func_get_smtp_server_error($smtp, 'SMTP authentication failed (SMTP AUTH method: '.(empty($config['Email']['smtp_auth_method']) ? 'AUTO' : $config['Email']['smtp_auth_method']).')',  'errno', 'errstr');
			$smtp->Close();
		}
	}
echo "<br><br><pre>";
print_r($result);
echo "</pre>";
?>
