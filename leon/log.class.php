<?php
class MyLog {
 private $file;
 private $lines = array();

 public function __construct($filename){
  $this->file = fopen($filename, "a+");
  $this->log_text("[ ".date("d-m-Y h:i:s")." ]");
 }
 public function __destruct(){
  fputs($this->file, join("", $this->lines));
  fclose($this->file);
 }
 public function log_text($line){
  $this->lines[] = $line."\n";
 }
 public function log_var($var){
  ob_start();
  var_dump($var);
  $content = ob_get_contents();
  ob_end_clean();
  $this->lines[] = $content."\n";
 }
}
?>
