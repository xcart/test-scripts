<?php
require_once("./auth.php");
require_once $xcart_dir."/include/xcartmailer.php";
x_load("mail");

$smtp = new XcartMailer();
$port = intval($config['Email']['smtp_port']);

	# Initiate connection.
	if (!$smtp->Connect($config['Email']['smtp_server'], $port)) {
		$result['message'] = func_get_smtp_server_error($smtp, 'Failed to connect to SMTP server', 'errno', 'errstr');
		$smtp->Close();
		var_dump($result);
		die("terminated");
	}

	# Send greeting.
	if (!$smtp->Hello()) {
		$result['message'] = func_get_smtp_server_error($smtp, 'Failed to complete SMTP handshake');
		$smtp->Close();
		var_dump($result);
		die("terminated");
	}

	# Perform SMTP authentication.
	if (!empty($config['Email']['smtp_user'])) {
		$smtp->setAuthMethod($config['Email']['smtp_auth_method']);
		if (!$smtp->Authenticate($config['Email']['smtp_user'], $config['Email']['smtp_password'])) {
			$result['message'] = func_get_smtp_server_error($smtp, 'SMTP authentication failed (SMTP AUTH method: '.(empty($config['Email']['smtp_auth_method']) ? 'AUTO' : $config['Email']['smtp_auth_method']).')');
			$smtp->Close();
		var_dump($result);
		die("terminated");
		}
	}



?>
