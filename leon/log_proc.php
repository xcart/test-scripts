<?php

function __log($var, $log_file_name = "mylog.txt") 
{
ob_start();
echo "--- Date : ".date("d-m-Y H:i:s",time())." --- <br>\r\n";
# our data
var_dump($var);
# /out data 
echo "------------------------------- <br>\r\n";
$content = ob_get_contents();
ob_end_clean();

$fh = fopen($log_file_name, "a+");
fwrite($fh, $content);
fclose($fh);
}
?>
