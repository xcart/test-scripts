<?php
if (extension_loaded('gd') || function_exists("gd_info")) {
	if (function_exists("gd_info")) {
		$gd_config = gd_info();
		echo "<table border=1>";
		echo "<tr><td>Parameter Name</td><td>Parameter Value</td></tr>";
		foreach($gd_config as $k=>$v){
			echo "<tr><td>".$k."</td>";
			if ($v === true) $content = "<font color=green>True<//font>";
			elseif ($v === false)$content = "<font color=red>False<//font>";
			else $content = $v;
			echo "<td>".$content."</td></tr>";
		}
		echo "</table>";
	}
}else{
echo "<font color='red'>GDLib extension not loaded</font>";
}
?>
