<?php

require "./auth.php";

x_load('tests');


function test_curl()
{
    $curl = func_find_executable('curl');
    if ($curl) {
        @exec(func_shellquote($curl)." --version", $output);
        if (!empty($output) && stristr($output[0],'ssl') && preg_match("/^curl\s+([\d\.]+)/", $output[0], $match)) {
            $tmp = explode('.', $match[1]);
            if ($tmp[0] > 6 && ($tmp[1] > 9 || ($tmp[1] == 9 && intval($tmp[2]) > 0)))
                return $output[0];
        }
    }
    return '';
}

var_dump(test_curl());

if (test_curl()) {

	var_export(func_https_request_curl("POST", "www.paypal.com:443"));

} else echo "failed.";

?>
