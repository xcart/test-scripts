<?php

function func_http_get_request($host, $post_url, $post_str, $post_cookies=array(), $user = false, $pass = false) {
    $hp = explode(':',$host);
    $timeout = 15;

    $cookie = "";

    $result = "";
    $header_passed = false;

    if (!isset($hp[1]) || !is_numeric($hp[1])) $hp[1] = 80;

    $host = $hp[1] == 80 ? $hp[0] : implode(':', $hp);

    $fp = fsockopen($hp[0], $hp[1], $errno, $errstr, $timeout);
    if (!$fp) {
        return array ("", "");
    }
    else {
        fputs ($fp, "GET $post_url?$post_str HTTP/1.0\r\n");
        fputs ($fp, "Host: $host\r\n");
        fputs ($fp, "User-Agent: Mozilla/4.5 [en]\r\n");
        if ($user)
            fputs ($fp, "Authorization: Basic ".base64_encode($user.($pass ? ":".$pass : ""))."\r\n");
        if (!empty($post_cookies))
            fputs ($fp, "Cookie: ".join('; ',$post_cookies)."\r\n");

        fputs ($fp,"\r\n");

        if (doubleval(phpversion()) >= 4.3) {
            @stream_set_timeout($fp, $timeout);
        }

        $http_header = array ();
        $http_header["ERROR"] = chop(fgets($fp,4096));
        $cookies = array ();
        while (!feof($fp)) {
            if (!$header_passed)
                $line = fgets($fp, 4096);
            else
                $result .= fread($fp, 65536);

            if ($header_passed == false && ($line == "\n" || $line == "\r\n")) {
                $header_passed = true;
                continue;
            }

            if ($header_passed == false) {
                $header_line = explode(": ", $line, 2);
                $header_line[0] = strtoupper($header_line[0]);
                $http_header[$header_line[0]] = chop($header_line[1]);

                if ($header_line[0] == 'SET-COOKIE')
                    array_push($cookies, chop($header_line[1]));
            }
        }

        fclose($fp);
    }

    return array($http_header, $result);
}

$host = "ecoliving.com.au";
$post_url = '/product.php';
$post_str = 'test_string';
func_http_get_request($host, $post_url, $post_str);
?>
