<?php

$count = isset($_GET['count']) && intval($_GET['count']) > 0 ? intval($_GET['count']) : 500;

echo "<html><body>";
echo "max_input_vars: " . ini_get('max_input_vars') . "<br/>";
echo "Count: " . $count . "<br/>";

echo "<form name='test' method='POST' action='" . $_SERVER['SCRIPT_NAME'] . "?count=" . $count . "'>";
for ($i=0;$i<$count;$i++) {
	if (($i % 50) == 0) echo "<br/>";
	$checked = isset($_POST['item'.$i]) ? "checked" : "";
	echo "<input type='checkbox' id='".$i."' name='item".$i."' ".$checked."/>"; 
}

echo "<br/><input type='submit' value='submit'/>";
echo "</form>";

$js = <<<js
<script type='text/javascript'>
function checkAll(C) {
for (i=0;i<C;i++)
document.test.elements[i].checked='1';
}
function uncheckAll(C) {
for (i=0;i<C;i++)
document.test.elements[i].checked=false;
}
</script>
js;

echo $js;
echo "<input type='button' value='Check all' onClick='javascript: checkAll(".$count.")'/>";
echo "<input type='button' value='Uncheck all' onClick='javascript: uncheckAll(".$count.")'/>";
echo "</body></html>";